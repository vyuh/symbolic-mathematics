# symbolic-mathematics

Symbolic Manipulation of Mathematical Objects and their representation.
I think work on this started in
[April 2018](https://twitter.com/pii_ke/status/990866487807303680). 

## PORTABILITY

I developed this on [Chicken Scheme](https://www.call-cc.org/).
I think,
- the file `mac.scm` is portable [R5RS](http://wiki.call-cc.org/man/4/The%20R5RS%20standard).
- files `demo.scm` and `repl.scm` do not work
  with `mit-scheme` unless
  `(interaction-environment)`
  is replaced with
  `system-global-environment`.

## COMMANDS

    csc repl.scm
    rlwrap ./repl # you can omit `rlwrap`
    csi < demo.scm
