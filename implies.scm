(define implies
  '(
    ((in x (cap A B)) (in x A))
    ((in x (cap A B)) (in x B))
    ((in x B) (in x (cup A B)))
    ((in x A) (in x (cup A B)))))
(define expand
  '(
    ((* (- x) (- y)) (* x y))
    ((* (- x) y) (- (* x y)))
    ((* x (- y)) (- (* x y)))
    ((square a) (* a a))
    ((cube a) (* a a))
    ((square (+ a b)) (+ (square a) (square b) (* 2 a b)))
    ((cube (+ a b)) (+ (cube a) (cube b) (* 3 (square a) b) (* 3 a (square b))))))
(define equivalent
  '(
    ((- a b) (sum a (- b)))
    ((diff a b) (abs (- a b)))
    ((square (sum a b)) (sum (square a) (square b) (prod 2 a b)))
    ((square (diff a b)) (sum (square a) (square b) (prod -2 a b)))))
