(define mac
 '((show (syntax-rules ()
          ((_ a)
           `(,@(latex a) #\newline))
          ((_ a b ...)
           `(,@(latex a) #\newline ,@(show b ...)))))
   (args (syntax-rules ()
          ((_ (a))
           '(a ")"))
          ((_ (a . rest))
           `("(" a "," ,@(args rest)))))
   (sum (syntax-rules ()
         ((_ a)
          `(,@(latex a)))
         ((_ a b ...)
          `(,@(latex a) "+" ,@(sum b ...)))))
   (product (syntax-rules ()
             ((_ a)
              `(,@(latex a)))
             ((_ a b ...)
              `(,@(latex a) ,@(product b ...)))))
   (latex
    (syntax-rules (define lambda
                          paren
                          brace
                          bracket
                          if
                          >
                          =
                          tan
                          -
                          cube
                          frac
                          +
                          cos
                          sin
                          tan
                          csc
                          sec
                          cot
                          asin
                          acos
                          atan
                          acsc
                          asec
                          acot
                          *
                          sqr
                          sqrt
                          pow
                          int
			  pi
                          der)
     ((_ pi)
      `("\\pi"))
     ((_ (brace a))
      `("\\left\\{" ,@(latex a) "\\right\\}"))
     ((_ (bracket a))
      `("\\left[" ,@(latex a) "\\right]"))
     ((_ (paren a))
      `("\\left(" ,@(latex a) "\\right)"))
     ((_ (define a
                 (lambda b
                  c)))
      `(,(symbol->string 'a) ,@(args b) "=" ,@(latex c)))
     ((_ (> a b))
      `(a ">" b))
     ((_ (if p
          y
          n))
      `("\\begin{cases}" y
                         "&"
                         "\\text{if }"
                         ,@(latex p)
                         "\\\\"
                         n
                         "&"
                         "\\text{otherwise}"
                         "\\end{cases}"))
     ((_ (int f x))
      `("\\int " ,@(latex f) " \\,\\mathrm{d}{" ,@(latex x) "}"))
     ((_ (int a b f x))
      `("\\int_{" ,@(latex a)
                  "}^{"
                  ,@(latex b)
                  "}"
                  ,@(latex f)
                  " \\,\\mathrm{d}{"
                  ,@(latex x)
                  "}"))
     ((_ (der f x))
      `("\\frac{\\mathrm{d}{" ,@(latex f) "}}{\\mathrm{d}{" ,@(latex x) "}}"))
     ((_ (der n f x))
      `("\\frac{\\mathrm{d}^{" ,@(latex n) "}{" ,@(latex f) "}}{\\mathrm{d}{" ,@(latex x) "^{" ,@(latex n) "}}}"))
     ((_ (cube a))
      `("{" ,@(latex a) "}" "^3"))
     ((_ (sqr a))
      `("{" ,@(latex a) "}" "^2"))
     ((_ (sqrt a))
      `("\\sqrt{" ,@(latex a) "}"))
     ((_ (sqrt n a))
      `("\\sqrt[" ,@(latex n) "]{" ,@(latex a) "}"))
     ((_ (pow (asin a) x))
      `("{\\sin^{-1}" ,@(latex a) "}^{" ,@(latex x) "}"))
     ((_ (pow (acos a) x))
      `("{\\cos{-1}" ,@(latex a) "}^{" ,@(latex x) "}"))
     ((_ (pow (atan a) x))
      `("{\\tan{-1}" ,@(latex a) "}^{" ,@(latex x) "}"))
     ((_ (pow (acsc a) x))
      `("{\\cosec{-1}" ,@(latex a) "}^{" ,@(latex x) "}"))
     ((_ (pow (asec a) x))
      `("{\\sec{-1}" ,@(latex a) "}^{" ,@(latex x) "}"))
     ((_ (pow (acot a) x))
      `("{\\cot{-1}" ,@(latex a) "}^{" ,@(latex x) "}"))
     ((_ (pow (sin a) x))
      `("{\\sin^{" ,@(latex x) "}" ,@(latex a) "}"))
     ((_ (pow (cos a) x))
      `("{\\cos^{" ,@(latex x) "}" ,@(latex a) "}"))
     ((_ (pow (tan a) x))
      `("{\\tan^{" ,@(latex x) "}" ,@(latex a) "}"))
     ((_ (pow (csc a) x))
      `("{\\mathrm{cosec}^{" ,@(latex x) "}" ,@(latex a) "}"))
     ((_ (pow (sec a) x))
      `("{\\sec^{" ,@(latex x) "}" ,@(latex a) "}"))
     ((_ (pow (cot a) x))
      `("{\\cot^{" ,@(latex x) "}" ,@(latex a) "}"))
     ((_ (pow a x))
      `("{" ,@(latex a) "}" "^{" ,@(latex x) "}"))
     ((_ (+ a ...))
      (sum a ...))
     ((_ (* a ...))
      (product a ...))
     ((_ (- a))
      `("-" ,@(latex a)))
     ((_ (- a b))
      `(,@(latex a) "-" ,@(latex b)))
     ((_ (frac a b))
      `("\\frac{" ,@(latex a) "}{" ,@(latex b) "}"))
     ((_ (asin x))
      `("{\\sin^{-1}" ,@(latex x) "}"))
     ((_ (acos x))
      `("{\\cos{-1}" ,@(latex x) "}"))
     ((_ (atan x))
      `("{\\tan{-1}" ,@(latex x) "}"))
     ((_ (acsc x))
      `("{\\mathrm{cosec}^{-1}" ,@(latex x) "}"))
     ((_ (asec x))
      `("{\\sec^{-1}" ,@(latex x) "}"))
     ((_ (acot x))
      `("{\\cot^{-1}" ,@(latex x) "}"))
     ((_ (sin (+ a ...)))
      `("\\sin{" "(" ,@(latex (+ a ...)) ")" "}"))
     ((_ (sin (- a ...)))
      `("\\sin{" "(" ,@(latex (- a ...)) ")" "}"))
     ((_ (sin a))
      `("\\sin{" ,@(latex a) "}"))
     ((_ (cos (+ a ...)))
      `("\\cos{" "(" ,@(latex (+ a ...)) ")" "}"))
     ((_ (cos (- a ...)))
      `("\\cos{" "(" ,@(latex (- a ...)) ")" "}"))
     ((_ (cos a))
      `("\\cos{" ,@(latex a) "}"))
     ((_ (tan (+ a ...)))
      `("\\tan{" "(" ,@(latex (+ a ...)) ")" "}"))
     ((_ (tan (- a ...)))
      `("\\tan{" "(" ,@(latex (- a ...)) ")" "}"))
     ((_ (tan a))
      `("\\tan{" ,@(latex a) "}"))
     ((_ (csc a))
      `("\\mathrm{cosec}{" ,@(latex a) "}"))
     ((_ (sec a))
      `("\\sec{" ,@(latex a) "}"))
     ((_ (cot a))
      `("\\cot{" ,@(latex a) "}"))
     ((_ (= a b))
      `(,@(latex a) "=" ,@(latex b)))
     ((_ (= a b c ...))
      `(,@(latex a) "=" ,@(latex (= b c ...))))
     ((_ a)
      '(a))))))
