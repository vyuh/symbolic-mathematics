(load "mac.scm")
(define repl
  (lambda ()
    (define b (read))
    (if (not (eof-object? b))
      (begin
        (eval `(letrec-syntax ,mac (for-each display (show ,b))) (interaction-environment))
        (repl)))))
(repl)
