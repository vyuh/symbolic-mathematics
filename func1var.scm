; FUNCTIONS OF ONE VARIABLE
(define inv-trig '(asin acos atan acsc asec acot))
(define trig '(sin cos tan csc sec cot))
(define logexp '(log exp (loga a) (expa a)))
(define alg '((pown n) sq cub sqrt))
(define other '(abs ceil floor sgn))

; LINEAR COMBINATION OF FUNC1VAR OBJECTS
(define (double u)
 `(* 2 ,u))

(define (halve u)
 `(* 1/2 ,u))

(define (linc scalers objects)
 `(+ ,@(map (lambda (a u)
             `(* ,a ,u))
            scalers
            objects)))

(define (scale a u)
 `(* ,a ,u))

(define (sub u v)
 `(- ,u ,v))

(define (sum objects)
 `(+ ,@objects))

(define (triple u)
 `(* 3 ,u))

; OTHER COMBINATIONS
(define (comp f g)
 `(,f (,g)))

(define (frac u v)
 `(/ ,u ,v))

(define (prod objects)
 `(* ,@objects))
