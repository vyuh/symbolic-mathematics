(define identitiy
        '(= (int (sqrt (- (sqr a) (sqr x))) x)
            (+ (* (frac x 2) (sqrt (- (sqr a) (sqr x))))
               (* (frac (sqr a) 2) (asin (frac x a)))
               C)))
(define (substitutor x y)
 (lambda (input)
  (if (eqv? input x)
   y
   input)))

(define (traverse a transform)
 (cond
  ((null? a)
   '())
  ((list? (car a))
   (cons (traverse (car a) transform) (traverse (cdr a) transform)))
  (else
   (cons (transform (car a)) (traverse (cdr a) transform)))))

(traverse identitiy (substitutor 'x 't))
