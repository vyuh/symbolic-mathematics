(load "mac.scm")
(define b
    '(for-each
      display
      (show #\newline
            (define max (lambda (x y) (if (> x y) x y)))
            (= (tan u) (frac (+ (cube x) (cube y)) (- x y)))
            (= (cos (- x)) (cos x))
            (= (sin (- x)) (- (sin x)))
            (= (tan (+ x y))
               (frac (+ (tan x) (tan y)) (- 1 (* (tan x) (tan y)))))
            (= (sin (+ x y)) (+ (* (sin x) (cos y)) (* (cos x) (sin y))))
            (= (cos (+ x y)) (- (* (cos x) (cos y)) (* (sin x) (sin y)))))))
(eval `(letrec-syntax ,mac ,b) (interaction-environment))
